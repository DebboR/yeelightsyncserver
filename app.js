import express from 'express'
import http from 'http'
import socketIO from 'socket.io'
import Sockets from './Sockets'

// CONSTANTS //
const PORT = 3000

// Servers
const app = express()
const httpServer = http.createServer(app)
const io = socketIO(httpServer)
Sockets(io)

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

// Listeners
httpServer.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`)
})