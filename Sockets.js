
const Sockets = (io) => {
    io.on('connection', (socket) => {
        log('New connection!', socket)

        // Socket events
        socket.on('disconnect', () => {
            log('Disconnected!', socket)
        })

        socket.on('newLampState', (state) => {
            log(`Got lamp state: ${JSON.stringify(state)}`, socket)
            io.emit('lampState', state)
        })
    })
}

const log = (message, socket) => {
    console.log(`[SOCKETS][Id: ${socket.id}] ${message}`)
}

export default Sockets